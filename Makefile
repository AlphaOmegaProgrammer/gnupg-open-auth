CC=gcc
CFLAGS=-std=c18 -fstack-protector-all -Werror -Wall -Wextra -pedantic -I./src -D_POSIX_C_SOURCE=200809L
CPPGLAGS=$(CFLAGS)

OBJECTS = bin/main.o \
	bin/error.o \
	bin/thread-accept.o \
	bin/thread-https.o \
	bin/api/get_key.o


# PHONY targets

.PHONY:
all: CFLAGS += -O3 -march=native
.PHONY:
all: bin/openauth

.PHONY:
clean:
	rm -rf bin
	mkdir -p bin/api

.PHONY:
debug: CFLAGS += -g -O0 -D__OPENAUTH_DEBUG=1
.PHONY:
debug: bin/openauth



# Real targets

bin/openauth: $(OBJECTS)
	$(CC) $(OBJECTS) -o bin/openauth -lodbc -lssl -lcrypto -L/root/gpgme/gpgme-1.14.0/src/.libs -lgpgme -lgpg-error -pthread

bin/%.o: src/%.c
	$(CC) $(CFLAGS) -c $< -o $@
