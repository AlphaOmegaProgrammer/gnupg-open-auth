With this project, I tried to make an OpenAuth system in C based on GnuPG and Postgres using ODBC. From what I remember, this was my first real attempt at something for production in C. I ended up never finishing this because the GnuPG library is absolute hell to work with.

I'd actually really like to revisit this some day, and probably rework it to be based on [SPICY](https://gitlab.com/AlphaOmegaProgrammer/spicy).
