#include <string.h>
#include <unistd.h>

// OpenSSL
#include <openssl/ssl.h>

// ODBC
#include <odbcinst.h>
#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>
#include <sqlucode.h>


// Internal
#include "configs/main.h"
#include "configs/api.h"

#include "api.h"
#include "error.h"
#include "thread-https.h"

void api_get_public_gpg_key_from_fingerprint(struct https_thread_data *https_thread_data){
	// Parse the fingerprint
	unsigned char gpg_public_key[GPG_PUBLIC_KEY_MAX_LENGTH] = {0};
	unsigned char binary_fingerprint[42];
	char *fingerprint = strstr(https_thread_data->data_pointer, "fingerprint=");
	if(fingerprint == NULL){
		api_error(
			https_thread_data,
			"Unable to find fingerprint",
			OPENAUTH_ERROR_ACTION_BREAKPOINT
		);

		return;
	}

	fingerprint += 12;
	if(strlen(fingerprint) < 40){
		api_error(
			https_thread_data,
			"Fingerprint must be 40 hex digits",
			OPENAUTH_ERROR_ACTION_BREAKPOINT
		);

		return;
	}

	memcpy(&binary_fingerprint[2], fingerprint, 40);
	binary_fingerprint[0] = '\\';
	binary_fingerprint[1] = 'x';

	// Get the public key from the database
	SQLRETURN query_return_status;
	SQLLEN binary_fingerprint_length = 42;
	SQLLEN gpg_public_key_length = GPG_PUBLIC_KEY_MAX_LENGTH;

	if(
		odbc_check_error(
			SQLBindParameter(
				https_thread_data->odbc_postgres_queries[ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT],
				1,
				SQL_PARAM_INPUT,
				SQL_C_CHAR,
				SQL_CHAR,
				binary_fingerprint_length,
				0,
				binary_fingerprint,
				binary_fingerprint_length,
				&binary_fingerprint_length
			),
			"Failed to bind fingerprint parameter:",
			SQL_HANDLE_STMT,
			https_thread_data->odbc_postgres_queries[ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT],
			OPENAUTH_ERROR_ACTION_NONE
		)
	 || odbc_check_error(
			SQLExecute(https_thread_data->odbc_postgres_queries[ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT]),
			"Failed to execute query:",
			SQL_HANDLE_STMT,
			https_thread_data->odbc_postgres_queries[ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT],
			OPENAUTH_ERROR_ACTION_NONE
		)
	 || odbc_check_error(
			SQLBindCol(
				https_thread_data->odbc_postgres_queries[ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT],
				1,
				SQL_C_BINARY,
				&gpg_public_key,
				GPG_PUBLIC_KEY_MAX_LENGTH,
				&gpg_public_key_length
			),
			"Failed to bind gpg public key column:",
			SQL_HANDLE_STMT,
			https_thread_data->odbc_postgres_queries[ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT],
			OPENAUTH_ERROR_ACTION_NONE
		)
	)
		return;

	query_return_status = SQLFetch(https_thread_data->odbc_postgres_queries[ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT]);
	if(query_return_status == SQL_NO_DATA){
		api_error(
			https_thread_data,
			"Key not found",
			OPENAUTH_ERROR_ACTION_BREAKPOINT
		);

		return;
	}

	if(
		odbc_check_error(
			query_return_status,
			"Failed to fetch gpg public key:",
			SQL_HANDLE_STMT,
			https_thread_data->odbc_postgres_queries[ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT],
			OPENAUTH_ERROR_ACTION_NONE
		)
	 || odbc_check_error(
			SQLCloseCursor(https_thread_data->odbc_postgres_queries[ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT]),
			"Failed to close cursor:",
			SQL_HANDLE_STMT,
			https_thread_data->odbc_postgres_queries[ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT],
			OPENAUTH_ERROR_ACTION_NONE
		)
	)
		return;

	// Finally, base64 encode it and respond
	printf("%li\n", gpg_public_key_length);

	long j;
	for(j=0; j<gpg_public_key_length; j++)
		printf("%X ", gpg_public_key[j]);

	printf("\n\n");

	gpgme_data_t gpgme_key_data = NULL;
	gpgme_check_error(
		gpgme_data_new_from_mem(
			&gpgme_key_data,
			(const char*)gpg_public_key,
			gpg_public_key_length,
			1
		),
		"gpgme_data_new_from_mem failed",
		OPENAUTH_ERROR_ACTION_NONE
	);

	gpgme_check_error(
		gpgme_data_set_encoding(
			gpgme_key_data,
			GPGME_DATA_ENCODING_BINARY
		),
		"Failed to set key data encoding to binary!",
		OPENAUTH_ERROR_ACTION_NONE
	);

	gpgme_check_error(
		gpgme_op_import(
			https_thread_data->gpgme_context,
			gpgme_key_data
		),
		"Importing GPG public key failed",
		OPENAUTH_ERROR_ACTION_NONE
	);

	gpgme_import_result_t gpgme_result = gpgme_op_import_result(https_thread_data->gpgme_context);

	printf(
		"Imports: %p\nConsidered: %i\nImported: %i\nRSA: %i\nUnchanged: %i\nNot Imported: %i\n\n",
		(void*)gpgme_result->imports,
		gpgme_result->considered,
		gpgme_result->imported,
		gpgme_result->imported_rsa,
		gpgme_result->unchanged,
		gpgme_result->not_imported
	);

//	gpgme_data_release(gpgme_key);
//	gpgme_set_armor(https_thread_data->gpgme_context, 1);

//	gpgme_check_error(
//		gpgme_data_new(&gpgme_key),
//		"gpgme_data_new failed"
//	);
/*
	gpgme_check_error(
		gpgme_op_export(
			https_thread_data->gpgme_context,
			NULL,
			0,
			gpgme_key
		),
		"Exporting GPG public key failed"
	);
*/
	strcpy(https_thread_data->write_buffer, "HTTP/1.1 200 Ok\r\nContent-Type: text/plain\r\n\r\n");
	https_thread_data->write_length = strlen(https_thread_data->write_buffer);

	unsigned long gpgme_data_read_result = gpgme_data_read(
		gpgme_key_data,
		&https_thread_data->write_buffer[https_thread_data->write_length],
		WRITE_BUFFER_SIZE - https_thread_data->write_length
	);

	printf("\n\n%lu\n", gpgme_data_read_result);

	if(gpgme_data_read_result < 1){
		perror("no key?");
		printf("Buffer:\n%s\n\n", https_thread_data->write_buffer);
		exit(1);
	}

	https_thread_data->write_length += gpgme_data_read_result;
/*
	gpgme_key_t gpgme_actual_key;
	gpgme_check_error(
		gpgme_get_key(
			https_thread_data->gpgme_context,
			NULL,
			&gpgme_actual_key,
			0
		),
		"Failed to get key from engine"
	);

	gpgme_check_error(
		gpgme_op_delete_ext(
			https_thread_data->gpgme_context,
			gpgme_actual_key,
			GPGME_DELETE_FORCE
		),
		"Removing key from engine failed"
	);

	gpgme_key_unref(gpgme_actual_key);
*/
	gpgme_data_release(gpgme_key_data);

	https_openssl_check_error(
		SSL_write(
			https_thread_data->client_ssl,
			https_thread_data->write_buffer,
			https_thread_data->write_length
		) == https_thread_data->write_length,
		"HTTPS response for get-key failed",
		OPENAUTH_ERROR_ACTION_NONE
	);
}
