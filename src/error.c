#include <signal.h>
#include <string.h>
#include <unistd.h>

// ODBC
#include <odbcinst.h>
#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>
#include <sqlucode.h>

// OpenSSL
#include <openssl/err.h>
#include <openssl/ssl.h>

// Internal
#include "error.h"
#include "thread-https.h"

unsigned char gpgme_check_error(gpgme_error_t gpgme_error, char* message, enum OPENAUTH_ERROR_ACTION action){
	if(gpgme_error == GPG_ERR_NO_ERROR)
		return 0;

#ifdef __OPENAUTH_DEBUG
	if(action == OPENAUTH_ERROR_ACTION_BREAKPOINT)
		kill(0, SIGTRAP);
#endif

	perror(message);

	const char* gpgme_error_string = gpgme_strsource(gpgme_error);
	write(2, gpgme_error_string, strlen(gpgme_error_string));
	write(2, "\n", 1);

	if(action == OPENAUTH_ERROR_ACTION_EXIT)
		exit(OPENAUTH_EXIT_GNUPG);

	return 1;
}

unsigned char https_openssl_check_error(int success, char *message, enum OPENAUTH_ERROR_ACTION action){
	if(success)
		return 0;

#ifdef __OPENAUTH_DEBUG
	if(action == OPENAUTH_ERROR_ACTION_BREAKPOINT)
		kill(0, SIGTRAP);
#endif

	perror(message);
	ERR_print_errors_fp(stderr);

	if(action == OPENAUTH_ERROR_ACTION_EXIT)
		exit(OPENAUTH_EXIT_OPENSSL);

	return 1;
}

unsigned char odbc_check_error(SQLRETURN success, char *message, SQLSMALLINT handle_type, SQLHANDLE handle, enum OPENAUTH_ERROR_ACTION action){
	if(SQL_SUCCEEDED(success))
		return 0;

#ifdef __OPENAUTH_DEBUG
	if(action == OPENAUTH_ERROR_ACTION_BREAKPOINT)
		kill(0, SIGTRAP);
#endif

	perror(message);

	SQLINTEGER	i = 0;
	SQLINTEGER	native;
	SQLCHAR		state[7];
	SQLCHAR		text[256];
	SQLSMALLINT	len;
	SQLRETURN	ret;

	do{
		ret = SQLGetDiagRec(handle_type, handle, ++i, state, &native, text, sizeof(text), &len);
		fprintf(stderr, "%s:%d:%d:%s\n", state, i, native, text);
	}while(ret == SQL_SUCCESS);

	if(action == OPENAUTH_ERROR_ACTION_EXIT)
		exit(OPENAUTH_EXIT_ODBC);

	return 1;
}



// api_error behaves differently than the other error handling functions
void api_error(struct https_thread_data *data, char *message, enum OPENAUTH_ERROR_ACTION action){
#ifdef __OPENAUTH_DEBUG
	if(action == OPENAUTH_ERROR_ACTION_BREAKPOINT)
		kill(0, SIGTRAP);
#endif

	strcpy(data->write_buffer, "HTTP/1.1 400 Bad Request\r\nContent-Type: text/plain\r\n\r\n");
	strcat(data->write_buffer, message);

	data->write_length = strlen(data->write_buffer);

	https_openssl_check_error(
		SSL_write(data->client_ssl, data->write_buffer, data->write_length) == data->write_length,
		"HTTPS API error response failed?!",
		action
	);
}


