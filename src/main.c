// OpenSSL
#include <openssl/err.h>
#include <openssl/ssl.h>

// ODBC
#include <odbcinst.h>
#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>
#include <sqlucode.h>

// Networking
#include <arpa/inet.h>
#include <sys/socket.h>

// pthread
#include <pthread.h>

// GnuPG
#include <gpgme.h>

// Other
#include <fcntl.h>
#include <locale.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Internal
#include "main.h"
#include "error.h"
#include "thread-accept.h"
#include "thread-https.h"

#include "configs/main.h"
#include "configs/https.h"
#include "configs/sql.h"


// I hate making global variables, but it's easier to deal with this way and saves memory
struct main_data main_data;

const char* odbc_postgres_queries[] = {
	"SELECT gpg_public_key FROM accounts WHERE gpg_key_fingerprint = ?"
};



void setup(){
	// Firstly, we need to set up some libraries before we can listen and allow any of the threads to do any work. This includes the GPGME library

	setlocale (LC_ALL, "");
	gpgme_check_version(NULL);
	gpgme_set_locale (NULL, LC_CTYPE, setlocale (LC_CTYPE, NULL));

	gpgme_check_error(
		gpgme_engine_check_version(GPGME_PROTOCOL_OPENPGP),
		"gpgme_engine_check_version() failed",
		OPENAUTH_ERROR_ACTION_EXIT
	);

	// And the ODBC library
	main_data.odbc.sql_environment = SQL_NULL_HENV;
	main_data.odbc.sql_connection = SQL_NULL_HDBC;

	odbc_check_error(
		SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &(main_data.odbc.sql_environment)),
		"SQLAllorHandle ENV Failed:",
		SQL_HANDLE_ENV,
		main_data.odbc.sql_environment,
		OPENAUTH_ERROR_ACTION_EXIT
	);

	odbc_check_error(
		SQLSetEnvAttr(main_data.odbc.sql_environment, SQL_ATTR_ODBC_VERSION, (void *) SQL_OV_ODBC3, 0),
		"SQLSetEnvAttr ODBC Version 3 Failed:",
		SQL_HANDLE_ENV,
		main_data.odbc.sql_environment,
		OPENAUTH_ERROR_ACTION_EXIT
	);

	odbc_check_error(
		SQLAllocHandle(SQL_HANDLE_DBC, main_data.odbc.sql_environment, &(main_data.odbc.sql_connection)),
		"SQLAllocHandle DBC Failed:",
		SQL_HANDLE_DBC,
		main_data.odbc.sql_connection,
		OPENAUTH_ERROR_ACTION_EXIT
	);

	odbc_check_error(
		SQLDriverConnect(main_data.odbc.sql_connection, NULL, (SQLCHAR*)"Driver="SQL_DRIVER";Server="SQL_SERVER";Port="SQL_PORT";Database="SQL_DATABASE";Uid="SQL_USERNAME";Pwd="SQL_PASSWORD, SQL_NTS, NULL, 0, NULL, SQL_DRIVER_COMPLETE),
		"SQLDriverConnect Failed:",
		SQL_HANDLE_DBC,
		main_data.odbc.sql_connection,
		OPENAUTH_ERROR_ACTION_EXIT
	);


	// Now, set up the HTTPS listener
	main_data.https.ssl_context = SSL_CTX_new(TLS_server_method());

	https_openssl_check_error(
		main_data.https.ssl_context != NULL,
		"SSL_CTX_new failed:",
		OPENAUTH_ERROR_ACTION_EXIT
	);

	https_openssl_check_error(
		SSL_CTX_set_min_proto_version(
			main_data.https.ssl_context,
			TLS1_2_VERSION
		) == 1,
		"SSL_CTX_set_min_proto_version failed",
		OPENAUTH_ERROR_ACTION_EXIT
	);

	https_openssl_check_error(
		SSL_CTX_use_certificate_file(
			main_data.https.ssl_context,
			HTTPS_LISTEN_SSL_CERT,
			SSL_FILETYPE_PEM
		) == 1,
		"SSL_CTX_use_certificate_file fialed",
		OPENAUTH_ERROR_ACTION_EXIT
	);

	https_openssl_check_error(
		SSL_CTX_use_PrivateKey_file(
			main_data.https.ssl_context,
			HTTPS_LISTEN_SSL_KEY,
			SSL_FILETYPE_PEM
		) == 1,
		"SSL_CTX_use_PrivateKey_file fialed",
		OPENAUTH_ERROR_ACTION_EXIT
	);

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = HTTPS_LISTEN_PORT;
	addr.sin_addr.s_addr = inet_addr(HTTPS_LISTEN_ADDRESS);

	main_data.https.socket = socket(AF_INET, SOCK_STREAM, 0);
	if(main_data.https.socket == -1){
		perror("Unable to create socket");
		exit(OPENAUTH_EXIT_HTTP);
	}

	if(bind(main_data.https.socket, (struct sockaddr*)&addr, sizeof(addr)) == -1){
		perror("Unable to bind");
		exit(OPENAUTH_EXIT_HTTP);
	}

	if(listen(main_data.https.socket, LISTEN_BACKLOG_LIMIT) == -1){
		perror("Unable to listen");
		exit(OPENAUTH_EXIT_HTTP);
	}


	// Now that the listener is set up, we can drop privileges
	struct passwd *passwd = getpwnam(PROCESS_USER);
	if(setgid(passwd->pw_gid) != 0){
		printf("Unable to drop group privileges!\n");
		exit(OPENAUTH_EXIT_DROP_PRIVILEGES);
	}

	if(setuid(passwd->pw_uid) != 0){
		printf("Unable to drop user privileges!\n");
		exit(OPENAUTH_EXIT_DROP_PRIVILEGES);
	}


	// And now that we've dropped privileges, we can set up the accept thread
	main_data.accept_cond = (pthread_cond_t)PTHREAD_COND_INITIALIZER;
	main_data.accept_mutex = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;

	main_data.accept_backlog_index = 0;
	main_data.accept_backlog_count = 0;

	if(pthread_create(&(main_data.accept_thread), NULL, accept_thread_function, NULL) != 0){
		perror("Failed to create HTTPS thread");
		exit(OPENAUTH_EXIT_ACCEPT_THREAD);
	}

	if(pthread_detach(main_data.accept_thread) != 0){
		perror("Failed to detach HTTPS thread");
		exit(OPENAUTH_EXIT_ACCEPT_THREAD);
	}

	// Then the HTTPS threads
	main_data.ready_https_thread_count = 0;

	unsigned long i;
	for(i=0; i<NUMBER_OF_HTTPS_THREADS; i++){
		if(pthread_create(&(main_data.https_threads[i]), NULL, https_listen_thread_function, NULL) != 0){
			perror("Failed to create HTTPS thread");
			exit(OPENAUTH_EXIT_HTTPS_THREAD);
		}

		if(pthread_detach(main_data.https_threads[i]) != 0){
			perror("Failed to detach HTTPS thread");
			exit(OPENAUTH_EXIT_HTTPS_THREAD);
		}
	}


	// And now we wait for all of the threads to be ready
	struct timespec wait;
	wait.tv_sec = 0;
	wait.tv_nsec = 10;

	while(main_data.ready_https_thread_count < NUMBER_OF_HTTPS_THREADS)
		nanosleep(&wait, NULL);
}

void cleanup(){
	// Clean up HTTP (when https->socket closes, all of the threads will exit)
	shutdown(main_data.https.socket, SHUT_RDWR);
	close(main_data.https.socket);
	SSL_CTX_free(main_data.https.ssl_context);

	struct timespec wait;
	wait.tv_sec = 0;
	wait.tv_nsec = 10;

	// Now wait for the threads to exit
	while(main_data.ready_https_thread_count > 0)
		nanosleep(&wait, NULL);


	// Clean up ODBC
	odbc_check_error(
		SQLDisconnect(main_data.odbc.sql_connection),
		"SQLDisconnect Failed",
		SQL_HANDLE_DBC,
		main_data.odbc.sql_connection,
		OPENAUTH_ERROR_ACTION_EXIT
	);

	odbc_check_error(
		SQLFreeHandle(SQL_HANDLE_DBC, main_data.odbc.sql_connection),
		"SQLFreeHandle DBC Failed",
		SQL_HANDLE_DBC,
		main_data.odbc.sql_connection,
		OPENAUTH_ERROR_ACTION_EXIT
	);

	odbc_check_error(
		SQLFreeHandle(SQL_HANDLE_ENV, main_data.odbc.sql_environment),
		"SQLFreeHandle ENV Failed",
		SQL_HANDLE_ENV,
		main_data.odbc.sql_environment,
		OPENAUTH_ERROR_ACTION_EXIT
	);
}


int main(){
	setup();

	for(;;){
		pause();
		write(1, "Main pause() done\n", 18);
	}

	cleanup();

	return OPENAUTH_EXIT_SUCCESS;
}
