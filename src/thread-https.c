#include <pthread.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "configs/gpgme.h"

#include "thread-https.h"
#include "api.h"
#include "error.h"

extern struct main_data main_data;
extern const char* odbc_postgres_queries[];

// This goes with "enum odbc_postgres_query_names" in main.h
void https_listen_thread_setup(struct https_thread_data *this){
	// Set up the GPGME context
	gpgme_check_error(
		gpgme_new(&(this->gpgme_context)),
		"gpgme_new() failed",
		OPENAUTH_ERROR_ACTION_EXIT
	);

	gpgme_check_error(
		gpgme_ctx_set_engine_info(
			this->gpgme_context,
			GPGME_PROTOCOL_OPENPGP,
			OPENAUTH_GPGME_BIN_PATH,
			OPENAUTH_GPGME_HOME_PATH
		),
		"Failed to set engine info for GPGME context",
		OPENAUTH_ERROR_ACTION_EXIT
	);

	// Prepare ODBC postgres queries
	unsigned char i;
	for(i=0; i<ODBC_POSTGRES_QUERY_COUNT; i++){
		odbc_check_error(
			SQLAllocHandle(
				SQL_HANDLE_STMT,
				main_data.odbc.sql_connection,
				&(this->odbc_postgres_queries[i])
			),
			"Failed to allocate ODBC statement handler",
			SQL_HANDLE_STMT,
			this->odbc_postgres_queries[i],
			OPENAUTH_ERROR_ACTION_EXIT
		);

		odbc_check_error(
			SQLPrepare(
				this->odbc_postgres_queries[i],
				(SQLCHAR*)odbc_postgres_queries[i],
				strlen(odbc_postgres_queries[i])
			),
			"Failed to prepare ODBC postgres statement",
			SQL_HANDLE_STMT,
			this->odbc_postgres_queries[i],
			OPENAUTH_ERROR_ACTION_EXIT
		);
	}
}

void* https_listen_thread_function(void *unused){
	unused = unused; // Make gcc shut up about "unused argument"

	struct https_thread_data this;
	https_listen_thread_setup(&this);

	main_data.ready_https_thread_count++;

	for(;;){
		pthread_mutex_lock(&(main_data.accept_mutex));

		while(!main_data.accept_backlog_count)
			pthread_cond_wait(&(main_data.accept_cond), &(main_data.accept_mutex));

		this.client_socket = main_data.accept_backlog[main_data.accept_backlog_index];
		main_data.accept_backlog_count--;
		main_data.accept_backlog_index++;
		if(main_data.accept_backlog_index >= ACCEPT_BACKLOG_LIMIT)
			main_data.accept_backlog_index = 0;

		pthread_mutex_unlock(&(main_data.accept_mutex));

		this.client_ssl = SSL_new(main_data.https.ssl_context);
		if(
			https_openssl_check_error(
				this.client_ssl != NULL,
				"Failed to SSL_new() for HTTPS client",
				OPENAUTH_ERROR_ACTION_NONE
			)
		 || https_openssl_check_error(
				SSL_set_fd(this.client_ssl, this.client_socket) == 1,
				"Failed to SSL_set_fd() for HTTPS client",
				OPENAUTH_ERROR_ACTION_NONE
			)
		 || https_openssl_check_error(
				SSL_accept(this.client_ssl) == 1,
				"Failed to SSL_accept() for HTTPS client",
				OPENAUTH_ERROR_ACTION_NONE
			)
		)
			goto https_event_end;

		this.read_length = SSL_read(this.client_ssl, &this.read_buffer, READ_BUFFER_SIZE);
		if(https_openssl_check_error(
			this.read_length > 0,
			"Failed to SSL_read for HTTPS client,",
			OPENAUTH_ERROR_ACTION_NONE
		))
			goto https_event_end;

		// If somehow the request is more than READ_BUFFER_SIZE bytes, we just truncate it and ignore the rest
		if(this.read_length == READ_BUFFER_SIZE)
			this.read_length = READ_BUFFER_SIZE - 1;

		this.read_buffer[this.read_length] = '\0';

		switch(this.read_buffer[0]){
			case 'G':
				if(
					this.read_buffer[1] != 'E'
				 || this.read_buffer[2] != 'T'
				 || this.read_buffer[3] != ' '
				 || this.read_buffer[4] != '/'
				 || this.read_buffer[5] != 'a'
				 || this.read_buffer[6] != 'p'
				 || this.read_buffer[7] != 'i'
				 || this.read_buffer[8] != '/'
				)
					goto https_event_end;

				this.api_call_pointer = &(this.read_buffer[9]);
				this.data_pointer = strchr(this.api_call_pointer, '?');
				if(this.data_pointer == NULL)
					goto https_event_end;

				this.data_pointer[0] = '\0';
				this.data_pointer++;

				char *end_pointer = strchr(this.data_pointer, ' ');
				if(end_pointer)
					goto https_event_end;

				end_pointer[0] = '\0';
			break;

			case 'P':
				if(
					this.read_buffer[1] != 'O'
				 || this.read_buffer[2] != 'S'
				 || this.read_buffer[3] != 'T'
				 || this.read_buffer[4] != ' '
				 || this.read_buffer[5] != '/'
				 || this.read_buffer[6] != 'a'
				 || this.read_buffer[7] != 'p'
				 || this.read_buffer[8] != 'i'
				 || this.read_buffer[9] != '/'
				)
					goto https_event_end;

				this.api_call_pointer = &(this.read_buffer[10]);
				this.data_pointer = strchr(this.api_call_pointer, ' ');
				if(this.data_pointer == NULL)
					goto https_event_end;

				this.data_pointer[0] = '\0';
				this.data_pointer++;
				this.data_pointer = strstr(this.data_pointer, "\r\n\r\n");
				if(this.data_pointer == NULL)
					goto https_event_end;

				this.data_pointer += 4;
			break;

			default:
				goto https_event_end;
		}


		if(!strcmp(this.api_call_pointer, "get-key"))
			api_get_public_gpg_key_from_fingerprint(&this);



https_event_end:
		SSL_shutdown(this.client_ssl);
		SSL_free(this.client_ssl);
		shutdown(this.client_socket, SHUT_RDWR);
		close(this.client_socket);
	}

	write(1, "HTTPS thread exit\n", 18);

	// Clean up GPGME context
	gpgme_release(this.gpgme_context);

	// Clean up ODBC statements
	unsigned char i;
	for(i=0; i<ODBC_POSTGRES_QUERY_COUNT; i++)
		odbc_check_error(
			SQLFreeHandle(SQL_HANDLE_STMT, this.odbc_postgres_queries[i]),
			"Failed to free ODBC statement handler",
			SQL_HANDLE_STMT,
			this.odbc_postgres_queries[i],
			OPENAUTH_ERROR_ACTION_EXIT
		);


	main_data.ready_https_thread_count--;
	return 0;
}
