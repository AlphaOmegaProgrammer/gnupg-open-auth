#define NUMBER_OF_HTTPS_THREADS	1

#define ACCEPT_BACKLOG_LIMIT	100000

#define HTTPS_LISTEN_ADDRESS	"0.0.0.0"
#define HTTPS_LISTEN_PORT		0

#define HTTPS_LISTEN_SSL_KEY	"/etc/letsencrypt/live/example.com/privkey.pem"
#define HTTPS_LISTEN_SSL_CERT	"/etc/letsencrypt/live/exmaple.com/fullchain.pem"
