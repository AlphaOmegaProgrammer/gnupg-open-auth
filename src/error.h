#ifndef __OPENAUTH_ERROR
#define __OPENAUTH_ERROR

// ODBC
#include <odbcinst.h>
#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>
#include <sqlucode.h>

// GnuPG
#include <gpgme.h>

// Internal
#include "main.h"



enum OPENAUTH_EXIT_CODES{
	OPENAUTH_EXIT_SUCCESS,
	OPENAUTH_EXIT_GNUPG,
	OPENAUTH_EXIT_ODBC,
	OPENAUTH_EXIT_OPENSSL,
	OPENAUTH_EXIT_HTTP,
	OPENAUTH_EXIT_ACCEPT_THREAD,
	OPENAUTH_EXIT_HTTPS_THREAD,
	OPENAUTH_EXIT_DROP_PRIVILEGES
};

enum OPENAUTH_ERROR_ACTION{
	OPENAUTH_ERROR_ACTION_NONE,
	OPENAUTH_ERROR_ACTION_BREAKPOINT,
	OPENAUTH_ERROR_ACTION_EXIT
};

unsigned char https_openssl_check_error(int, char*, enum OPENAUTH_ERROR_ACTION);
unsigned char gpgme_check_error(gpgme_error_t, char*, enum OPENAUTH_ERROR_ACTION);
unsigned char odbc_check_error(SQLRETURN, char*, SQLSMALLINT, SQLHANDLE, enum OPENAUTH_ERROR_ACTION);


void api_error(struct https_thread_data*, char*, enum OPENAUTH_ERROR_ACTION);
#endif
