#ifndef __OPENAUTH_MAIN
#define __OPENAUTH_MAIN

// ODBC
#include <odbcinst.h>
#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>
#include <sqlucode.h>

// OpenSSL
#include <openssl/err.h>
#include <openssl/ssl.h>

// GnuPG
#include <gpgme.h>


#include "configs/main.h"
#include "configs/https.h"

struct odbc{
	SQLHENV		sql_environment;
	SQLHDBC		sql_connection;
};

struct https{
	int			socket;
	SSL_CTX		*ssl_context;
};

// This goes with "const char* odbc_postgres_queries[]" in thread.https.c
enum odbc_postgres_query_names{
	ODBC_POSTGRES_SELECT_GPG_PUBLIC_KEY_FROM_FINGERPRINT,

	ODBC_POSTGRES_QUERY_COUNT
};

struct https_thread_data{
	SSL *client_ssl;
	gpgme_ctx_t gpgme_context;
	SQLHSTMT odbc_postgres_queries[ODBC_POSTGRES_QUERY_COUNT];

	int client_socket, read_length, write_length;
	char read_buffer[READ_BUFFER_SIZE], write_buffer[WRITE_BUFFER_SIZE], *api_call_pointer, *data_pointer;
};

struct main_data{
	unsigned long ready_https_thread_count;

	struct https https;
	struct odbc odbc;

	pthread_t accept_thread;
	pthread_t https_threads[NUMBER_OF_HTTPS_THREADS];

	pthread_cond_t accept_cond;
	pthread_mutex_t accept_mutex;
	unsigned long accept_backlog_index;
	unsigned long accept_backlog_count;
	int accept_backlog[ACCEPT_BACKLOG_LIMIT];
};
#endif
