#include <pthread.h>

#include <errno.h>
#include <poll.h>
#include <stdio.h>
#include <time.h>
#include <sys/socket.h>
#include <unistd.h>


#include "main.h"
#include "configs/https.h"

extern struct main_data main_data;


void* accept_thread_function(void* unused){
	unused = unused; // Make gcc shut up about "unused argument"

	int poll_return;
	unsigned long accept_backlog_index = 0;

	struct pollfd pollfd;
	pollfd.fd = main_data.https.socket;
	pollfd.events = POLLIN;
	pollfd.revents = 0;

	struct timespec wait = {
		.tv_sec = 0,
		.tv_nsec = 1000000
	};

	for(;;){
		poll_return = poll(&pollfd, 1, 5);

		if(poll_return < 0){
			perror("poll error?");
			break;
		}

		if(poll_return > 0){
			pthread_mutex_lock(&(main_data.accept_mutex));

			while(poll(&pollfd, 1, 0) > 0){
				main_data.accept_backlog[accept_backlog_index] = accept(main_data.https.socket, NULL, NULL);

				if(main_data.accept_backlog[accept_backlog_index] < 0){
					if(errno == EAGAIN || errno == EWOULDBLOCK)
						continue;

					if(errno == EINVAL)
						write(1, "HTTPS socket closed\n", 20);
					else
						perror("Accept failed");

					break;
				}

				main_data.accept_backlog_count++;
				accept_backlog_index++;

				pthread_cond_signal(&(main_data.accept_cond));

				if(accept_backlog_index >= ACCEPT_BACKLOG_LIMIT)
					accept_backlog_index = 0;

				while(main_data.accept_backlog_count == ACCEPT_BACKLOG_LIMIT)
					nanosleep(&wait, NULL);
			}

			pthread_mutex_unlock(&(main_data.accept_mutex));
		}
	}

	return 0;
}
